import pygame, sys, time
from pygame.locals import *

pygame.init()

FPS = 30
fpsClock=pygame.time.Clock()

##Tamanho do display
size = (640, 480)

##Cria o display
DISPLAYSURF=pygame.display.set_mode(size)

##Título do display
pygame.display.set_caption('Behaviour_test')

##Carregando a imagem do fundo
background = pygame.image.load('background.jpg')

##Carrega a imagem que vai mexer
coord = pygame.image.load('bola2.png')

''' coord_x e coord_y são as coordenadas iniciais da imagem que iremos movimentar, onde 
    (0,0) é o canto superior esquerdo e os eixos crescem para a direita e para baixo'''
coord_x = 0
coord_y = 0

##quando direction=None a imagem ficará parada
direction=None

def move(direction, coord_x, coord_y):
    ''' pygame tem as constantes padrões pras teclas;
        essas que eu usei são correspondentes as setinhas do teclado '''
    if direction:
        if direction == K_UP:
            coord_y -= 20
            
        elif direction == K_DOWN:
            coord_y += 20
            
        if direction == K_LEFT:
            coord_x -= 20
            
        elif direction == K_RIGHT:
            coord_x += 20
            
    return coord_x, coord_y

while True:
    ##'Desenha' o display
    DISPLAYSURF.blit(background,(0,0))
    DISPLAYSURF.blit(coord,(coord_x,coord_y))

    ''' event é uma propriedade da biblioteca;
        ".get" pega um evento da fila, todo evento é um objeto 
        que possui o atributo type.
        keydown e keyup são os eventos quando as teclas estão 
        sendo pressionadas e quando são soltas.'''
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

        if event.type == KEYDOWN:
            direction = event.key
                
        if event.type == KEYUP:
            if (event.key == direction):
                direction = None
    
    ##atualiza as coordenadas
    coord_x, coord_y = move(direction, coord_x, coord_y)

    pygame.display.update()
    fpsClock.tick(FPS)